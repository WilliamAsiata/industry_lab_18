package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {

	public static void main(String [] args){
		new ExcelNew(550).start();
	}

	private String output;
	private int classSize;
	ArrayList<String> firstNameList = new ArrayList<String>();
	ArrayList<String> surnameList = new ArrayList<String>();


	public ExcelNew(int classSize) {
		this.classSize = classSize;
	}

	private void start() {
		readNamesFiles("FirstNames.txt", firstNameList);
		readNamesFiles("Surnames.txt", surnameList);
		getClassMarks();
		writeMarkstoFile();
	}

	private void writeMarkstoFile() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"))){
			bw.write(output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readNamesFiles(String fileName, ArrayList<String> nameList) {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))){
			String line;
			while((line = br.readLine())!= null){
				nameList.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getClassMarks() {
		output="";
		for(int i = 1; i <= classSize; i++){
            output += getStudentMarks(i);
        }
	}

	private String getStudentMarks(int i) {
		String student = "";
		student += getStudentId(i);
		student += getStudentName();
		//Student Skill
		int randStudentSkill = (int)(Math.random()*101);
		//Labs//////////////////////////
		int numLabs = 3;
		int[] labScheme = {5, 15, 25, 65};
		for(int j = 0; j < numLabs; j++){
			student += getMark(randStudentSkill, labScheme);
		}
		//Test/////////////////////////
		int[] testScheme = {5, 20, 65, 90};
		student += getMark(randStudentSkill, testScheme);
		///////////////Exam////////////
		if(randStudentSkill <= 7){
			int randDNSProb = (int)(Math.random()*101);
			if(randDNSProb <= 5){
				student += ""; //DNS
			}else{
				student = markGenerator(40, 0);
			}
		} else{
			int[] examScheme = {7, 20, 60, 90};
			student += getMark(randStudentSkill, examScheme);
		}
		//////////////////////////////////
		student += "\n";
		return student;
	}

	private String getStudentName() {
		int randFNIndex = (int)(Math.random()*firstNameList.size());
		int randSNIndex = (int)(Math.random()*surnameList.size());
		return "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
	}

	private String getStudentId(int i) {
		String student = "";

		if(i/10 < 1){
            student += "000" + i;
        }else if (i/100 < 1){
            student += "00" + i;
        }else if (i/1000 < 1){
            student += "0"+i;
        }else{
            student += i;
        }
		return student;
	}

	private String getMark(int randStudentSkill, int[] gradeScheme) {
		String student = "";
		if(randStudentSkill <= 5){
			student += markGenerator(40 ,0);
		}else if (skillIsBetween(gradeScheme[0], gradeScheme[1], randStudentSkill)){
            student += markGenerator(10,40); // [40,49]
        }else if(skillIsBetween(gradeScheme[1], gradeScheme[2], randStudentSkill)){
            student += markGenerator(20,50); // [50,69]
        }else if(skillIsBetween(gradeScheme[2], gradeScheme[3], randStudentSkill)){
            student += markGenerator(20,70); // [70,89]
        } else{
            student += markGenerator(11,90); //[90,100]
        }
		student += "\t";
		return student;
	}

	private String markGenerator(int a, int b) {
		return "" + ((int)(Math.random()*a) + b);
	}

	private boolean skillIsBetween(int lowerBound, int upperBound, int randStudentSkill) {
		return (randStudentSkill > lowerBound) && (randStudentSkill <= upperBound);
	}

}